#!/bin/bash 

# 结束tomcat
cd /mnt/tomcat8/bin/
./shutdown.sh
# 结束 root 用户 执行的所有 java 进程的命令（除了本身grep的这个之外）,免得没能成功结束tomcat
# 因为要重启，所以这个也不用结束了
# ps -ef | grep java | grep root | grep -v grep |awk '{print $2}' | xargs --no-run-if-empty kill -9

##########################

# wm
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wm-*.jar
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
wget http://down.zvo.cn/wangmarket/version/v6.0/wm-2.38.jar

# wangmarket
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket-*.jar
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
wget http://down.zvo.cn/wangmarket/version/v6.0/wangmarket-6.0.4.jar


# 客服插件的客服模板安装
mkdir /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/static/plugin/kefu/template/ -p
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/static/plugin/kefu/template/*
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/static/plugin/kefu/template/
wget http://down.zvo.cn/wangmarket/plugin/kefu/template_v1.0.zip -O template.zip
yum -y install unzip
unzip template.zip
rm -rf template.zip

# form
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.form-*.jar
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
wget http://down.zvo.cn/wangmarket/plugin/form/wangmarket.plugin.form-1.8.jar

# htmlSeparate
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.htmlSeparate-*.jar
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
wget http://down.zvo.cn/wangmarket/plugin/htmlSeparate/wangmarket.plugin.htmlSeparate-1.13.jar

# 客服坐席
if ls /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.kefuzuoxi-*.jar 2>&1;then
	rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.kefuzuoxi-*.jar
	wget http://down.zvo.cn/wangmarket/plugin/kefuzuoxi/wangmarket.plugin.kefuzuoxi-1.4.1.jar
fi

# 客服
if ls /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.kefu-*.jar 2>&1;then
	rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.kefu-*.jar
	wget http://down.zvo.cn/wangmarket/plugin/kefu/wangmarket.plugin.kefu-1.1.jar
fi

##########################
# 启动tomcat，也就是--重启
reboot
