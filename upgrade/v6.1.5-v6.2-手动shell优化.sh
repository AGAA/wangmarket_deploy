# 结束tomcat
cd /mnt/tomcat8/bin/
./shutdown.sh
# 结束 root 用户 执行的所有 java 进程的命令（除了本身grep的这个之外）,免得没能成功结束tomcat
# 因为要重启，所以这个也不用结束了
# ps -ef | grep java | grep root | grep -v grep |awk '{print $2}' | xargs --no-run-if-empty kill -9


# upgrade
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.upgrade-*.jar
wget http://down.zvo.cn/wangmarket/plugin/upgrade/wangmarket.plugin.upgrade-1.2.4.jar
# 增加相关class文件
mkdir /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/com/xnx3/wangmarket/
mkdir /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/com/xnx3/wangmarket/plugin/
mkdir /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/com/xnx3/wangmarket/plugin/upgrade/
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/com/xnx3/wangmarket/plugin/upgrade/Update.class
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/com/xnx3/wangmarket/plugin/upgrade/
wget http://down.zvo.cn/wangmarket/plugin/upgrade/wangmarket_UpdateClass/Update.class?v=1.2.2 -O /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/com/xnx3/wangmarket/plugin/upgrade/Update.class

# 启动tomcat
cd /mnt/tomcat8/bin/
./startup.sh