#!/bin/bash 

# 结束tomcat
cd /mnt/tomcat8/bin/
./shutdown.sh
# 结束 root 用户 执行的所有 java 进程的命令（除了本身grep的这个之外）,免得没能成功结束tomcat
# ps -ef | grep java | grep root | grep -v grep |awk '{print $2}' | xargs --no-run-if-empty kill -9


############## 基础支持包等更新 ###############
# 更新wangmarket
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket-*.jar
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
wget http://down.zvo.cn/wangmarket/version/v6.0/wangmarket-6.0.jar
# 更新基础支持包 wm.jar
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wm-*.jar
wget http://down.zvo.cn/wangmarket/version/v6.0/wm-2.33.jar


# 启动tomcat
cd /mnt/tomcat8/bin/
./startup.sh
