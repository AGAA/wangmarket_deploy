#!/bin/bash 

# 结束tomcat
cd /mnt/tomcat8/bin/
./shutdown.sh
# 结束 root 用户 执行的所有 java 进程的命令（除了本身grep的这个之外）,免得没能成功结束tomcat
# 因为要重启，所以这个也不用结束了
# ps -ef | grep java | grep root | grep -v grep |awk '{print $2}' | xargs --no-run-if-empty kill -9


############支持包##############

# translate
# cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
# rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.translate-*.jar
# wget http://down.zvo.cn/wangmarket/plugin/translate/wangmarket.plugin.translate-1.1.jar

# wangmarket
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket-*.jar
wget http://down.zvo.cn/wangmarket/version/v6.5/wangmarket-6.5.1.jar

############## 数据库方面更新 ###############
# 数据库IP
database_ip=`java -cp ~/properties.jar Properties -path=/mnt/tomcat8/webapps/ROOT/WEB-INF/classes/application.properties -get database.ip`
# 数据库名
database_name=`java -cp ~/properties.jar Properties -path=/mnt/tomcat8/webapps/ROOT/WEB-INF/classes/application.properties -get database.name`
# 数据库登录用户名
database_username=`java -cp ~/properties.jar Properties -path=/mnt/tomcat8/webapps/ROOT/WEB-INF/classes/application.properties -get spring.datasource.username`
# 数据库登录密码
database_password=`java -cp ~/properties.jar Properties -path=/mnt/tomcat8/webapps/ROOT/WEB-INF/classes/application.properties -get spring.datasource.password`
# 端口号
applicationdatabasesourceurl=$(cat /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/application.properties | grep spring.datasource.url)
database_port=$(echo $applicationdatabasesourceurl | grep -oP '(?<=:)\d+(?=/)')
####### 数据库执行相关命令
# system 表中将 STATIC_RESOURCE_PATH 的值是 //res.zvo.cn/ 设置为 //res.cms.zvo.cn/
mysql -h $database_ip -P $database_port -u$database_username -p$database_password $database_name -e "UPDATE system SET value='//res.cms.zvo.cn/' WHERE name = 'STATIC_RESOURCE_PATH' AND value = '//res.zvo.cn/'"

##########################

# 重启tomcat
wget https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-build-wangmarketcms/raw/master/shell/restart_tomcat.sh -O /mnt/tomcat8/bin/restart_tomcat.sh && chmod -R 777 /mnt/tomcat8/bin/restart_tomcat.sh && nohup /mnt/tomcat8/bin/restart_tomcat.sh &
